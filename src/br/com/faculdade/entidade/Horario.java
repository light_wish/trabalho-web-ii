package br.com.faculdade.entidade;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Horario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idHorario;
	
	@NotNull(message = "Informe a data")
	@Temporal(TemporalType.TIMESTAMP)
	private Date horarioAula;
	
	@NotNull(message = "Informe o dia da semana")
	@Size(max = 10, message = "Maximo de caracteres permitido: 10")
	private String diaSemana;
	
	@ManyToMany(mappedBy = "horarios")
	private List<Turma> turmas;

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	public Integer getIdHorario() {
		return idHorario;
	}

	public void setIdHorario(Integer idHorario) {
		this.idHorario = idHorario;
	}

	public Date getHorarioAula() {
		return horarioAula;
	}

	public void setHorarioAula(Date horarioAula) {
		this.horarioAula = horarioAula;
	}

	public String getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}
}
