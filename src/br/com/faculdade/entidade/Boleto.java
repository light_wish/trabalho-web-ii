package br.com.faculdade.entidade;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Boleto {

	@Id
	private Integer codigoBoleto;
	
	@NotNull(message = "Informe o valor")
	private Double valor;
	
	@NotNull(message = "Informe a data")
	@Temporal(TemporalType.TIMESTAMP)
	private Date vencimento;
	
	private Double valorPago;
	
	private Date dataPagamento;

	@ManyToOne
	@JoinColumn(name = "matricula") //Nome da FK
	private Aluno aluno; 

	public Integer getCodigoBoleto() {
		return codigoBoleto;
	}

	public void setCodigoBoleto(Integer codigoBoleto) {
		this.codigoBoleto = codigoBoleto;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public Double getValorPago() {
		return valorPago;
	}

	public void setValorPago(Double valorPago) {
		this.valorPago = valorPago;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
}
