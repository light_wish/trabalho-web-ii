package br.com.faculdade.entidade;

import java.util.*;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Aluno {
	
	@Id
	private Integer matricula;
	
	@NotNull(message = "Informe o nome")
	@Size(max = 50, message = "Maximo de caracteres permitido: 50")
	private String nome;
	
	@NotNull(message = "Informe o cpf")
	@Size(max = 14, message = "Maximo de caracteres permitido: 14")
	private String cpf;
	
	@Size(max = 50, message = "Maximo de caracteres permitido: 50")
	private String email;
	
	@OneToMany(mappedBy = "aluno")
	private List<Boleto> boletos;
	
	@ManyToMany
	@JoinTable(name = "AlunoTurma",
		joinColumns = @JoinColumn(name = "matricula"),
			inverseJoinColumns = @JoinColumn(name = "idTurma"))
	private List<Turma> turmas;
	
	public List<Turma> getTurmas() {
		return turmas;
	}
	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Boleto> getBoletos() {
		return boletos;
	}
	public void setBoletos(List<Boleto> boletos) {
		this.boletos = boletos;
	}
	
	
}
