package br.com.faculdade.entidade;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Curso {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private short idCurso;
	
	@NotNull(message = "Informe o nome")
	@Size(max = 20, message = "Maximo de caracteres permitido: 20")
	private String nome;
	
	@OneToMany(mappedBy = "curso")
	private List<Turma> turmas;

	public short getIdCurso() {
		return idCurso;
	}

	public void setIdCurso(short idCurso) {
		this.idCurso = idCurso;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}
	
	
}
