package br.com.faculdade.entidade;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Turma {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private short idTurma;
	
	@NotNull(message = "Informe o nome")
	@Size(max = 20, message = "Maximo de caracteres permitido: 20")
	private String nome;
	
	@ManyToOne
	@JoinColumn(name = "idCurso")
	private Curso curso;
	
	@ManyToMany(mappedBy = "turmas")
	private List<Aluno> alunos;
	
	@ManyToMany
	@JoinTable(name = "TurmaHorario",
			joinColumns = @JoinColumn(name = "idTurma"),
			inverseJoinColumns = @JoinColumn(name = "idHorario"))
	private List<Horario> horarios;
	

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public List<Horario> getHorarios() {
		return horarios;
	}

	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}

	public short getIdTurma() {
		return idTurma;
	}

	public void setIdTurma(short idTurma) {
		this.idTurma = idTurma;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
	
}
